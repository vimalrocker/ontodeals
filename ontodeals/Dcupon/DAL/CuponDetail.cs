//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Dcupon.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class CuponDetail
    {
        public int id { get; set; }
        public int CategoriesId { get; set; }
        public int WebsitesId { get; set; }
        public string Title { get; set; }
        public int ImageurlId { get; set; }
        public string Description { get; set; }
        public int CouponType { get; set; }
        public int RedirectType { get; set; }
        public string RedirectUrl { get; set; }
        public string CuponCode { get; set; }
        public int IsActive { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public int SubcategoriesID { get; set; }
    }
}
